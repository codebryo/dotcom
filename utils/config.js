export function slugRoutesFromPosts(posts) {
  return Object.values(posts).reduce((routes, post) => {
    routes.push(`/p/${post.attributes.slug}`)
    return routes
  }, [])
}
