// Turn a date like 03121986 into a valid JS date
export function parsePostDate(dateStr) {
  const sanitizedDate = dateStr.replace(/([^\d])/g, '') // Replace everything that's not a digit
  return new Date(
    parseInt(sanitizedDate.substr(4, 4), 10),
    parseInt(sanitizedDate.substr(2, 2), 10) - 1,
    parseInt(sanitizedDate.substr(0, 2), 10)
  )
}

// Sort all posts by date and return a fresh posts object in the same structure
export function sortPostsByDate(posts) {
  const keysToSort = Object.keys(posts)

  keysToSort.sort((a, b) => {
    const dateA = parsePostDate(a.split('_')[0])
    const dateB = parsePostDate(b.split('_')[0])

    return dateB - dateA
  })
  const sortedPosts = {}
  keysToSort.map((key) => {
    sortedPosts[key] = posts[key]
  })
  return sortedPosts
}

// Get the first post
export function pickFirstPost(posts) {
  const allPosts = Object.keys(posts)
  const firstPostKey = allPosts[0]
  return posts[firstPostKey]
}

// Get the last Post
export function pickLastPost(posts) {
  const allPosts = Object.keys(posts)
  const lastPostKey = allPosts[allPosts.length - 1]
  return posts[lastPostKey]
}

// Get up to 3 most recent posts
export function pickRecentPosts(posts, maxPosts = 3) {
  const allPosts = Object.keys(posts)
  const availablePosts = allPosts.length - 1 // Don't show the latest post
  const loops = availablePosts >= maxPosts ? maxPosts : availablePosts
  const recentPosts = []

  for (let i = 1; i <= loops; i++) {
    const postKey = allPosts[i]
    recentPosts.push(posts[postKey].attributes)
  }

  return recentPosts
}
