# dotcom

[![Netlify Status](https://api.netlify.com/api/v1/badges/323b39dc-6183-45bb-b6e9-341e8a95a6af/deploy-status)](https://app.netlify.com/sites/quizzical-babbage-0c32a4/deploys)

## Preperation:

Make sure Netlify is installed globally

```shell
npm install -g netlify-cli
```

Run `ntl link` to link the local project to the actual netlify project.
Now all netlify commands can be executed.

## Development:

Leverage the `ntl dev:exec` command to execute Netlify Lamda functions.

Scripts can be found in `/scripts` folder.
