const faunadb = require('faunadb')
const getId = require('./utils/get-id')

const q = faunadb.query

const client = new faunadb.Client({
  secret: process.env.FAUNADB_SERVER_SECRET,
})

/**
 *
 * @param {*} currentData
 * @param {*} dataToMerge
 *
 * Merge the post meta and take care of special cases
 */
function mergePostMeta(currentData, dataToMerge) {
  const resolver = q.Lambda(
    ['claps', 'currentClaps', 'clapsToAdd'],
    q.Add(q.Var('currentClaps'), q.Var('clapsToAdd'))
  )

  return q.Merge(currentData, dataToMerge, resolver)
}

exports.handler = (event, context, callback) => {
  const clientData = JSON.parse(event.body)
  const postID = getId(event.path)

  function requestFailed(error) {
    callback(error, {
      statusCode: 400,
      body: JSON.stringify(error),
    })
  }

  client
    .query(q.Get(q.Match(q.Index('posts_meta_by_id'), postID)))
    .then((response) => {
      const ref = response.ref
      const mergedData = mergePostMeta(response.data, clientData)

      client
        .query(q.Update(ref, { data: mergedData }))
        .then((response) => {
          callback(null, {
            statusCode: 200,
            body: JSON.stringify(response),
          })
        })
        .catch((error) => {
          requestFailed(error)
        })
    })
    .catch((error) => {
      requestFailed(error)
    })
}
