---
title: How to work from home successfully

id: fbf9f47b-0774-55c1-9a67-b07c269dc6c6
image:
  url: https://images.unsplash.com/photo-1502672260266-1c1ef2d93688?ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=80
  author: Anna Popović <a href="https://unsplash.com/@pperkins" target="_blank">@pperkins</a>
slug: fbf9f47b-0774-55c1-9a67-b07c269dc6c6-working-from-home
published: 16.02.2020
updated: 16.02.2020
tags:
  - thoughts
  - culture
  - working remote
---

Due to recent world events working from home presents itself as a necessity, but also as a viable option for businesses. I am working from home most of my career, and I want to share some of my experience in the hope to help others. From my experience, there are two types of people (considering a country is not on lockdown) when it comes to work from home.

1. I can imagine to work when I am at home
2. I can absolutely not work at home

Type 2 can also work out great in a remote setting, leveraging infrastructure like coworking-spaces or coffee shops. But that's something for a different post.

So what if you have never really tried it out and can imagine to work at home, or did a few times but it did not work out as expected? Then this guide is for you:

## The workplace

Working from home can be challenge, especially in the beginning. We'll visit a few common gotchas and traps that await everyone working from home, but for now let's focus on where the work should happen.

Let me start by saying the couch is not right spot to spend a full work day on. Besides physiological reasons, "chilling" on the couch is just not the ride position to get actual work done, especially if other people or even kids might be around.

Find a place that works well, where you can sit, focus and get things done. Either you have a extra office or room at home, or if space is small the bed room is often a great place to set up camp, as people usually don't spend most of their day in the bed room. As a positive side effect, the audio situation for video calls is really great in bed rooms as the linen absorbs a lot of noises. (I expect you won't sit in bed all day, but actually get some kind of desk in there).

Make sure this is a place where you can work distraction free, have a good decent chair (living room chairs often work well) and wifi connection. A desk can be improvised very fast if you don't have one.

### What about kids

I have a daughter myself (she's 4 right now), and was working from home since she was born already. Let me tell you, that it was not easy and everyone struggles. Rule number one — it's okay that you want to spend time with them. They are your kids and when you are in an office you usually don't have the luxury of spending so much time with them. It's important though you create boundaries. Don't let the door open and let them distract you all the time, but step away from your workplace and actively spend time with them if you can. Jumping from work to kid to work, or doing either half assed is not great. This will frustrate both of you. You're kid is frustrated as you don't really spend time with them, and you will be frustrated as you don't get work done.

Simple signals like a closed door can go really far and help to create a boundary for the both of you.

## The right equipment

This is not a list of all the important things you need to buy to be successful, but a more general list to set you up for success.

1. Proper Wifi and power cables

   Get some proper wireless network going if don't already have one. Having a slow connection can be very frustrating and will not lead to great result. Also, make sure you have power available to charge your laptop.

2. A bottle of water

   Stay hydrated. You'll get more work done if you drink enough and if you have a water supply ready at where you sit. If you need to get up fro a fresh glass of water every 20 minutes, you might lose focus of the task you were working on, or start chatting with your significant other.

3. Noise Canceling Headphones

   Can't praise this kind of product enough. Having a device that cuts out noise and allows you to listen to your favourite tunes to keep you productive is amazing. That's probably the most expensive investment, but for me worth every cent.

4. A decent camera

   If you plan to do video calls with others, a decent camera goes a long way in making that experience more pleasant. If you have a recent device, chance is good that the camera works quite well. The worst experiences I had are with cameras that are filming from a very low angle. You don't want to look into anyones nostrils while doing a call. In that case get a external camera. It's worth it.

## Common Gotchas

There are also a bunch of gotchas waiting for you when working from home.

**Not leveraging focus mode**

That's a more general tip I guess, but when at home it's especially important to leverage focus mode. The internet is full of distractions, and there are even more at home. When you reach the point of focus, where you are dashing through things with full concentration. These modes are so important to get a lot of work done. Don't let these moments go to waste.

**Working from home is not Home work**

One of the things we don't think about when in the office is home work. All the household stuff like washing cloth, cooking, cleaning, etc.

When we are at home, these things are way more present. As it needs to be done, there is no way around it. But do one thing at a time. Don't do a little of this on the side and a little of the other on the side. For example I really enjoy cooking, and one of the perks when working from home is the ability to eat fresh made food. So when I cook, I cook and don't work on the side.

**Take a Break**

Taking breaks is important in the office and it's also important at home. Juse because you can work 8 hour shifts in one sitting, doesn't mean you should. No one can be productive for 8 hours. (Even 4 hours for me can be super hard). Take breaks and step away from work. It's important for you and important for your family.

## Ending the day

This is one of the hardest things, I still haven't mastered it fully, so be aware of this. At home, work is just a few steps away.

- Let me answer that Slack message
- Let me respond to that email
- Let me quickly fix that

When going to an office, your work day often ends when leaving the office. At home your office is always there. It's important to end the day at one point though if you don't want to burn your self out. Your mind needs some recreation time, and the work will be right there the next day.

If you notice that ending work is not easy, you might need a ritual to end it. When working in an office the commute is often the ritual that gets you ready for work and lets the day end. At home you maybe want to take a short walk to have the work day end

## Conclusion

It's not easy to work from home, and especially now amidst the COVID-19 crisis when it happens all of the sudden many companies are not in the spot to actually support proper work from home. If you know someone who's working from home or in a remote setting, ask them or reach out to me [on Twitter](https://twitter.com/codebryo). Reach out to people who have experience with this, and maybe you and your company come out stronger on the other side.
