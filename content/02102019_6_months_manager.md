---
title: What I learned in 6 months from engineer to manager

id: 909ffde5-ebc4-4936-839d-c67d18b657b8
image:
  url: https://images.unsplash.com/photo-1483213097419-365e22f0f258?ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=80
  author: Brendan Church <a href="https://unsplash.com/@bdchu614" target="_blank">@bdchu614</a>
slug: 909ffde5-ebc4-4936-839d-c67d18b657b8-6-months-manager
published: 02.10.2019
updated: 02.10.2019
tags:
  - career
  - engineering
---

It's now a little over 6 months since I officially transitioned into an engineering managers role. I want to share some of my learnings and thoughts, in hope to shed some light on this for the interested.

## 1. It's a different career

I have seen it happen at various places where the longest lasting engineers by a certain point in time , were **"promoted"** to a managers position. In rare occasions  people were excelling as a result of this opportunity, but I've also seen people failing miserably and eventually quit.

A good friend of mine was actually working for one of  these long lasting engineers that got promoted. This new manager was not  a particular soft spoken individual — never shy of a fight to push his own agenda. 
As manager this person became quite choleric and hostile towards his reports. While it was possible to ignore or counter him as a peer, it was impossible now to say anything. People started quitting, and the team got into such bad disarray that the manager was actually "demoted" back to engineer.

The damage was done by then. My friend had quit to pursue other opportunities, and rightfully so.

I can attest to the fact that being a manager is indeed a different career than being an engineer. My day to day work has shifted dramatically

## 2. Having a mentor is crucial

Luckily I had two great mentors and one in particular who helped me to make the final move from IC (Individual Contributor) to Manager. I am really thankful for all she has done for me, and I learned a lot. She moved on to another company as first time VPE and that's incredibly exciting.
My other mentor is an experienced VPE already and helped me become better at everything I was doing from the early days on at Codeship.

One book I got recommended before making the final jump was "Leading Snowflakes". Probably not the best title in todays times as snowflake has kind of a negative annotation, but the book itself was really good and I can very much **recommend it**. One thing in particular mentioned in this book was to write down dilemmas brought to me; my reaction and the solution I offered. Then I would ask other managers on what they would have done.

Having discussions about very particular issues was incredibly helpful and it's a practice I will continue to do going forward, as it's always good to reflect on past decisions.

## 3. Relationships are a great foundation

At my current company I was "promoted" form within the team I worked with for a long time. I had great relationships with my colleagues, where we also talked a lot about private stuff. So I wasn't sure how my relationships would change under the circumstance that they would now report to me.

It turned out that it actually worked quite well. We continued to talk about private stuff, and on work things I had new opportunities to have their backs and support them. I tried to do the things I missed as an engineer. Simplify some meetings, be very transparent and candid about everything and so on. We actually moved forward quite well, and I while we went through a pretty rocky patch as the wider team where we even lost a few people, all my reports stayed at the company and we moved forward. This would not have been possible if we would have had worse relationships.

## 4. You'll do less engineering

The other thing I learned was how little engineering I would eventually be able to do. I guess though this was the result of me learning a lot and the challenging times, where I got more reports from other teams and everything was in constant move. It was hard to focus. But regardless, it's fair to say that anyone who goes down management will do less engineering as a result. Everyone has to decide for them selves if that's something they are willing to do.
I started to do some side projects again, like this blog and play around more with Vue and Nuxt to keep my dev mind sharp, but it's for sure a very different approach than writing code every day.

## 5. Delayed results and late gratification

The most interesting change I have witnessed as being a manager was how delayed gratification for a job done well is felt. When I developed something and it worked, it's instant gratification. I did it; all tests passed; deployed it to the server; everything still works. It's always very instant. Not so with managing. Things will take some time.
Setting people up for success, accompanying them along the way and seeing them succeed with work done over a couple of weeks is very different. To me it feels even more gratifying as I see the person succeeding. Only know, if you are someone who needs this constant little wins to stay motivated... it might feel very different in management. I feel successful if someone else is successful because of the work I did. It's very rewarding to me.

## Conclusion

Overall it was very natural progression to me, and I do not regret to have taken the step. I thank all the people who helped me along the way. Having people you can trust and are willing to support you in order to see you succeed are the best people to have. Doing the same for others can be rewarding on levels writing a line of code never will. At least for me.
