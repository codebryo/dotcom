# The Magic Scripts

All scripts should be run from the project root.

## Create a new post

To create a new post run:

```shell
node scripts/newpost.js
```

Then provide a title, and you've got a new post.
You'll see an output like this

```shell
Post Created
============
title: Post Title

id: fbf9f47b-1234-55c1-7890-b07c269dc6c6

Don't forget to create the post_meta on faundadb
$ ntl dev:exec node scripts/faunaCreatePostMeta.js fbf9f47b-1234-55c1-7890-b07c269dc6c6
```

So execute the command to tell faunaDB in advance about the new post
