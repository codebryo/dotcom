const faunadb = require('faunadb')

const postId = process.argv[2]

if (!postId) return console.log('Please pass the id of the post')

const q = faunadb.query

const client = new faunadb.Client({
  secret: process.env.FAUNADB_SERVER_SECRET,
})

async function createPostsMeta(id) {
  const data = {
    id,
    claps: 0,
  }

  try {
    const response = await client.query(
      q.Create(q.Collection('posts_meta'), { data })
    )
    console.log(JSON.stringify(response, null, 2))
  } catch (e) {
    console.log('Post Creation on Fauna Failed')
  }
}

createPostsMeta(postId)
