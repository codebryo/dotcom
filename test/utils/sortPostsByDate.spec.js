import { sortPostsByDate } from '~/utils'

describe('sortPostsByDate', () => {
  test('sort from latest to earliest', () => {
    const posts = {
      '01022019_oldest': 3,
      '02042020_newest': 1,
      '03122019_middle': 2,
    }
    const sortedPosts = sortPostsByDate(posts)

    expect(Object.values(sortedPosts)).toEqual([1, 2, 3])
  })
})
