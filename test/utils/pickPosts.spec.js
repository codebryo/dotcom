import { pickFirstPost, pickLastPost, pickRecentPosts } from '~/utils'

const postFactory = function (amount = 1) {
  const posts = {}

  for (let i = 1; i <= amount; i++) {
    posts[`${i}`] = {
      attributes: {
        index: i,
      },
      body: i,
    }
  }

  return posts
}

describe('pickFirstPost', () => {
  test('returns only the first post from an object list', () => {
    const posts = postFactory(2)
    expect(pickFirstPost(posts).body).toBe(1)
  })
})

describe('pickLastPost', () => {
  test('returns only the last post from an object list', () => {
    const posts = postFactory(5)
    expect(pickLastPost(posts).body).toBe(5)
  })
})

describe('pickRecentPosts', () => {
  describe('by default', () => {
    test('returns up to 3 posts starting at nr 2', () => {
      const posts = postFactory(5)
      const recentPosts = pickRecentPosts(posts)
      expect(recentPosts.length).toBe(3)
      expect(recentPosts).toEqual([{ index: 2 }, { index: 3 }, { index: 4 }])
    })

    test('will load fewer posts if there are not enough', () => {
      const posts = postFactory(2)
      const recentPosts = pickRecentPosts(posts)
      expect(recentPosts.length).toBe(1)
    })
  })

  test('allows to override the default of 3', () => {
    const posts = postFactory(5)
    const recentPosts = pickRecentPosts(posts, 2)
    expect(recentPosts.length).toBe(2)
  })
})
