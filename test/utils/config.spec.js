import { slugRoutesFromPosts } from '~/utils/config'

describe('slugRoutesFromPosts', () => {
  test('will return an array of slug routes', () => {
    const posts = {
      a: {
        attributes: {
          slug: 1,
        },
      },
      b: {
        attributes: {
          slug: 2,
        },
      },
      c: {
        attributes: {
          slug: 3,
        },
      },
    }

    const routes = slugRoutesFromPosts(posts)
    expect(routes).toMatchInlineSnapshot(`
      Array [
        "/p/1",
        "/p/2",
        "/p/3",
      ]
    `)
  })
})
