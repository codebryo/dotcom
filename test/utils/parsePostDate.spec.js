import { parsePostDate } from '~/utils'

describe('parsePostDate', () => {
  test('will parse the custom date format', () => {
    const dates = [
      '03121986',
      '03.12.1986',
      '03-12-1986',
      '03_12_1986',
      '03  - - 12 -_-*foo1986',
    ]
    const expected = new Date(1986, 11, 3)

    dates.map((dateStr) => expect(parsePostDate(dateStr)).toEqual(expected))
  })
})
